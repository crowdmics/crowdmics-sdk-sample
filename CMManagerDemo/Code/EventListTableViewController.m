//
//  EventListTableViewController.m
//  CMManagerDemo
//
//  Created by Adam Gessel on 9/30/15.
//  Copyright © 2015 Crowd Mics. All rights reserved.
//

#import "EventListTableViewController.h"
#import "CMManager.h"
#import "CMEvent.h"
#import "EventTableViewCell.h"
#import "EventViewController.h"

@interface EventListTableViewController ()

@property (nonatomic, strong) NSMutableArray *eventsArray;
@property (nonatomic, weak) CMEvent *selectedEvent;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation EventListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Events";
    
    self.eventsArray = [NSMutableArray new];
    
    [[CMManager sharedSession] setDiscoveryDelegate:self];
    [[CMManager sharedSession] setLicenseDelegate:self];
    [[CMManager sharedSession] startEventDiscovery];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.eventsArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"eventCell";
    EventTableViewCell *cell = (EventTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = (EventTableViewCell *)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    CMEvent *event = self.eventsArray[indexPath.row];
    cell.eventNameLabel.text = event.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    id selectedEvent = self.eventsArray[indexPath.row];
    
    if ([selectedEvent hasPasscode]) {
        [[CMManager sharedSession]connectToEventWithUserFirstName:@"SDK" lastName:@"User"
                                                             uuid:[[NSUUID UUID] UUIDString]
                                                         passcode:@"123" andEvent:selectedEvent];
    } else {
        [[CMManager sharedSession]connectToEventWithUserFirstName:@"SDK" lastName:@"User"
                                                             uuid:[[NSUUID UUID] UUIDString]
                                                         passcode: nil
                                                         andEvent:selectedEvent];

    }
    
    [_activityIndicator setHidden:NO];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"eventViewSegue"]) {
        EventViewController *eventViewController = [segue destinationViewController];

        [eventViewController setEvent:self.selectedEvent];
    }
}

#pragma mark - Crowd Mics Manager Delegate

- (void)didFindEvent:(NSDictionary *)event {
    NSLog(@"did find event : %@", event);
    if (![self.eventsArray containsObject:event]) {
        [self.eventsArray addObject:event];
    }
    
    [self.tableView reloadData];
}

- (void)didRemoveEvent:(NSDictionary *)event {
    NSLog(@"did remove event : %@", event);
}

- (void)didConnectToEvent:(CMEvent *)event withClientController:(CMClientProtocolController *)clientController {
    [_activityIndicator setHidden:YES];
    self.selectedEvent = event;
    [self performSegueWithIdentifier:@"eventViewSegue" sender:self];
}

- (void)connectionToEventFailed:(CMControlServerResponse *)event {
    NSLog(@"failed to connect to event : %@", event);
}

- (void)connectionToEventTimedOut {
    NSLog(@"Timed out!");
}

#pragma mark - Crowd Mics Manager Delegate

- (void)licenseVerificationSucceeded {
    NSLog(@"License Verification succeeded!");
}
- (void)licenseVerificationError {
    NSLog(@"License Verification error!");
}
- (void)licenseVerificationFailed {
    NSLog(@"License Verification failed :(");
}

@end
