//
//  EventViewController.m
//  CMManagerDemo
//
//  Created by Adam Gessel on 10/1/15.
//  Copyright © 2015 Crowd Mics. All rights reserved.
//

#import "EventViewController.h"

@interface EventViewController ()
@property (strong, nonatomic) IBOutlet UILabel *eventNameLabel;
@property (strong, nonatomic) IBOutlet UIButton *requestToTalkButton;
@property (strong, nonatomic) IBOutlet UILabel *liveLabel;
@property (assign, nonatomic) BOOL isSpeaking;

@property (assign, nonatomic) BOOL hasSentRequestToTalk;

@end

@implementation EventViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.eventNameLabel.text = self.event.name;
    
    [self.requestToTalkButton addTarget:self action:@selector(request) forControlEvents:UIControlEventTouchUpInside];
    [[CMManager sharedSession] setTalkDelegate:self];

    [self.liveLabel setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)request {
    if (self.isSpeaking) {
        [self stopSpeaking];
        [[CMManager sharedSession] stopRequestToTalk];
        [self.requestToTalkButton setTitle:@"Request to Talk" forState:UIControlStateNormal];
    } else if (self.hasSentRequestToTalk) {
        [[CMManager sharedSession] stopRequestToTalk];
        [self.requestToTalkButton setTitle:@"Request to Talk" forState:UIControlStateNormal];
    } else {
        [[CMManager sharedSession] requestToTalk];
        [self.requestToTalkButton setTitle:@"Cancel Request" forState:UIControlStateNormal];
        [[CMManager sharedSession] stopSpeaking];
    }
    
    self.hasSentRequestToTalk = !self.hasSentRequestToTalk;
}

- (void)didReceiveAcceptTalkRequest:(id<CMClientTalkService>)clientTalkService {
    // We can now speak
    [self startSpeaking];

    // Show 'Mic is Live'
    [self.liveLabel setHidden:NO];
}

- (void)didSendRequestToTalk:(CMControlServerResponse*)response {
    // Do something
}

- (void)didReceiveRejectTalkRequest:(id<CMClientTalkService>)clientTalkService {
    [self stopSpeaking];
}

- (void)didReceiveBlockVoiceChatMessage:(id<CMClientTalkService>)clientTalkService {
    [self stopSpeaking];
}

- (void)didReceiveUnblockVoiceChatMessage:(id<CMClientTalkService>)clientTalkService {
    [self startSpeaking];
}

- (void)didReceiveFinishTalkRequest:(id<CMClientTalkService>)clientTalkService {
    [self stopSpeaking];
}

- (void)didCancelRequestToTalk {
    self.hasSentRequestToTalk = NO;
    [self stopSpeaking];
}

- (void)startSpeaking {
    [self.liveLabel setHidden:NO];
    self.isSpeaking = YES;
    [[CMManager sharedSession] speak];
}

- (void)stopSpeaking {
    [self.liveLabel setHidden:YES];
    self.isSpeaking = NO;
    [[CMManager sharedSession] stopSpeaking];
}

@end
