//
//  EventViewController.h
//  CMManagerDemo
//
//  Created by Adam Gessel on 10/1/15.
//  Copyright © 2015 Crowd Mics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMEvent.h"
#import "CMManager.h"

@interface EventViewController : UIViewController<CMManagerEventTalkDelegate>

@property(weak, nonatomic) CMEvent* event;

@end
