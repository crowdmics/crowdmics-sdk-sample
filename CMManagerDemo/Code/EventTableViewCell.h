//
//  EventTableViewCell.h
//  CMManagerDemo
//
//  Created by Adam Gessel on 10/1/15.
//  Copyright © 2015 Crowd Mics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *eventNameLabel;

@end
