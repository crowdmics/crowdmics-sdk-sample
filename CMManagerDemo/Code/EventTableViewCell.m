//
//  EventTableViewCell.m
//  CMManagerDemo
//
//  Created by Adam Gessel on 10/1/15.
//  Copyright © 2015 Crowd Mics. All rights reserved.
//

#import "EventTableViewCell.h"

@implementation EventTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
