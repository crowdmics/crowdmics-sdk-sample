//
//  AppDelegate.h
//  CMManagerDemo
//
//  Created by Adam Gessel on 9/30/15.
//  Copyright © 2015 Crowd Mics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

